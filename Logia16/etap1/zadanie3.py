# -*- coding: utf-8 -*-

from math import *
from skoki import *

""" Zadanie 4: Ornament
    Treść:
        Napisz jednoparametrową procedurę/funkcję ornament, po wywołaniu której
        na środku ekranu powstanie rysunek motywu takiego, jak poniżej.
        Parametr określa liczbę zielonych liści tworzących ornament i
        może przyjmować wartości od 3 do 16. Odległość pomiędzy środkami
        skrajnych liści jest stała i wynosi 500. Wszystkie odcinki tworzące
        pojedynczy liść mają długość a lub 2a. Odległość między środkami dwóch
        sąsiednich liści wynosi 10a.
"""

# Stałe
maksymalna_wartosc = 16
minimalna_wartosc = 3
odleglosc_miedzy_skrajnymi = 500

def rysuj_element_ornamentu(a, h):
    """ Funkcja rysuje element mozaiki w zależności od poziomu
    """

    turtle.penup()
    turtle.forward(2*h/3)
    turtle.pendown()

    turtle.begin_fill()
    turtle.right(150)
    for i in range (0, 3):
        turtle.forward(2*a)

        for j in range (0, 2):
            turtle.left(90)
            turtle.forward(a)

        turtle.right(30)
        turtle.forward(a)

        turtle.right(120)
        turtle.forward(a)

        turtle.left(30)
        turtle.forward(a)

        for j in range (0, 3):
            turtle.left(60)
            turtle.forward(a)
            turtle.right(120)
            turtle.forward(a)

        turtle.left(60)
        turtle.forward(a)

        turtle.left(30)
        turtle.forward(a)

        turtle.right(120)
        turtle.forward(a)

        turtle.right(30)
        turtle.forward(a)

        turtle.left(90)
        turtle.forward(a)
        turtle.left(90)
        turtle.forward(2*a)

        turtle.right(120)

    turtle.left(30)
    turtle.end_fill()

    turtle.penup()
    turtle.backward(2*h/3)
    turtle.pendown()

def ornament (ilosc_lisci):
    """ Funkcja rysująca ciąg ornamentu
        Wzory na wysokość: http://matematyka.pisz.pl/strona/856.html
        h = 0.5*5a*sqrt(3)
    """
    if ilosc_lisci < minimalna_wartosc or ilosc_lisci > maksymalna_wartosc:
        print ("Złe parametry. Liczba musi być z przedziału [%d..%d]" % (minimalna_wartosc, maksymalna_wartosc))

    a = odleglosc_miedzy_skrajnymi / ilosc_lisci / 10
    h = 0.5 * 5 * a * sqrt(3)
    turtle.pen(fillcolor="lightgreen")
    turtle.left(90)

    # Idź do pozycji domowej
    skocz_o(-odleglosc_miedzy_skrajnymi/2, 0)
    [x0, y0] = turtle.pos()
    for i in range (0, ilosc_lisci):
        if i % 2 != 0:
            turtle.right(180)

        skocz_do([x0 + i * a * 10, y0])
        rysuj_element_ornamentu(a, h)

        if i % 2 != 0:
            turtle.right(180)

turtle.speed(5000)
ornament(7)
input()
