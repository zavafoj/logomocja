import turtle

def skocz_do(pozycja):
    """ Funkcja przenoszenia żółwia z podniesieniem
        1. Przenieś do pozycji absolutnej
    """
    turtle.penup() # Podnieś pisak
    turtle.setpos(pozycja) # Ustaw pozycję
    turtle.pendown() # Opuść pisak

def skocz_o(dx, dy):
    """ Funckja przeniesienia o dx na osi X i dy na osi Y
        Funkcja dodaje dx do współrzędnej x i odejmuje dy od wsp. y

    """
    [x, y] = turtle.pos() # Zapisz obecną pozycję
    x = x + dx # Przesuń się o długość dx w prawo
    y = y - dy # Przesuń się o długość dy do góry
    skocz_do([x, y])
