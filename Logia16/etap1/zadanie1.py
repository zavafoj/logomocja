# -*- coding: utf-8 -*-

import math
from skoki import *

""" Zadanie 1: Motyw
    Treść:
        Napisz bezparametrową procedurę/ funkcję motyw, po wywołaniu której
        na środku ekranu powstanie rysunek taki, jak poniżej.
        Wysokość rysunku wynosi 480.
"""

# Stałe:
bok_obrazka = 480
# Długość boku największego z 9-ciu kwadratów:
bok_bloku = bok_obrazka/3
# Długość najmniejszego kwadratu:
podstawa = 0.2*bok_bloku
# Połowa przekątnej najmniejszego kwadratu:
podstawa_grzebienia = 0.5*podstawa*math.sqrt(2)

def skocz_do(pozycja):
    """ Funkcja przenoszenia żółwia z podniesieniem
        1. Przenieś do pozycji absolutnej
    """
    turtle.penup() # Podnieś pisak
    turtle.setpos(pozycja) # Ustaw pozycję
    turtle.pendown() # Opuść pisak

def skocz_o(dx, dy):
    """ Funckja przeniesienia o dx na osi X i dy na osi Y
        Funkcja dodaje dx do współrzędnej x i odejmuje dy od wsp. y

    """
    [x, y] = turtle.pos() # Zapisz obecną pozycję
    x = x + dx # Przesuń się o długość dx w prawo
    y = y - dy # Przesuń się o długość dy do góry
    skocz_do([x, y])

def rysuj_grzebien(boki):
    """ Funkcja rysująca jeden grzebień obwoluty
        1. Żółw idzie prosto, rysując linię o długości boku
        2. Żółw cofa się do pozycji początkowej
        3. Żółw rysuje harmonijkę wypełniając brzegi

        Atrybuty:
            boki - ilość boków (małych kwadracików)
    """

    dlugosc = podstawa * boki   # Ustal pożądaną długość boku kwadratu
    for i in range(0, 4): # Dla każdego z boku kwadratu [0, 1, 2, 3]
        # Część 1:
        pozycja = turtle.pos() # Pobierz pozycję startową
        turtle.forward(dlugosc) # Idź do przodu o długość boku

        # Część 2:
        skocz_do(pozycja) # Skocz do poprzedniej pozycji

        # Część 3:
        turtle.begin_fill() # Rozpocznij wypełnianie
        for i in range(0, boki): # Dla każdego z kwadracika narysuj grzebień:
            turtle.right(45) # Obróć w prawo o 45 stopni
            turtle.forward(podstawa_grzebienia) # Idź o długość podstawa_grzebienia
            turtle.left(90) # Obróć w lewo o 90 stopni
            turtle.forward(podstawa_grzebienia) # Idź o długość podstawa_grzebienia
            turtle.right(45) # Obróć w prawo o 45 stopni (poz. wyjściowa)

        turtle.right(90) # Obróć o 90 stopni w prawo (poz. wyjściowa)
        turtle.end_fill() # Zakończ wypełnianie

def rysuj_blok(boki, glebokosc):
    """ Funckja rysująca pojedynczy blok z określoną głębokością
        1. Głębokość określa maksymalny skok na zewnątrz kwadratu
        2. Z każdym wykonaniem iteracji głębokość się zmniejsza
           (wychodzimy na zewnątrz kwadratu)
        3. Zwiększ liczbę kwadracików w boku o 2
        4. Wywołaj samą z siebie ze zmienionymi parametrami
    """

    if glebokosc <= 0: # Jeżeli uzyskano odpowiednią głębokość, wyjdź
        return

    rysuj_grzebien(boki) # Rysuj grzebień

    glebokosc = glebokosc - 1 # Zmniejsz głębokość
    boki = boki + 2 # Dodaj po 2 boki z każdej strony
    skocz_o(-podstawa, -podstawa)
    rysuj_blok(boki, glebokosc) # Rysuj blok zewnętrzny z nowymi parametrami

def motyw():
    """ Główna funkcja programu:
        1. Rysuj kwadratową obwolutę
        2. Skacz po centrach kwadratów [0..8]
        3. Jeżeli id_kwadratu jest parzyste -> głębokość = 3
        4. Jeżeli id_kwadratu jest nieparzyste -> głębokość = 2
    """
    # Część 1:
    skocz_o(-bok_obrazka/2, -bok_obrazka/2) # Cofnij się od środka o połowę długości boków
    [x0, y0] = turtle.pos() # Zapisz pozycję bazową
    for i in range (0, 4): # Rysuj kwadrat
        turtle.forward(bok_obrazka)
        turtle.right(90)

    # Ustaw kolor wypełnienia na czarny
    turtle.pen(fillcolor="black")
    turtle.speed(5000)

    for kwadrat in range (0, 9): # Dla każdego dużego kwadratu
        # Uwaga, jeżeli masz problem z działaniami w układzie współrzednych,
        # przeczytaj https://pl.wikipedia.org/wiki/Układ_współrzędnych_kartezjańskich

        # Ustaw nową pozycje x:
        # Do pozycji domowej x dodaj bok bloku razy indeks kwadratu (0, 1 albo 2)
        # Uwaga! % jest operatorem reszty z dzielenia!
        # Dodaj połowę długości boku (aby znaleźć się na środku kwadratu)
        # Odejmij połowę długości podstawy (żeby kwadrat o boku podstawy był na środku)
        x_srodka = x0 + (kwadrat % 3) * bok_bloku + bok_bloku/2 - podstawa/2
        # Ustaw nową pozycje y:
        # Od pozycji domowej y odejmij indeks/3 razy bok kwadratu
        # int(x) określa dzielenie całkowite (bez ułamków)
        # Następnie odejmij połowę boku bloku i połowę podstawy (aby znaleźć się na środku)
        y_srodka = y0 - int(kwadrat / 3) * bok_bloku - bok_bloku/2 + podstawa/2
        # Skocz do środka obecnego kwadratu:
        skocz_do([x_srodka, y_srodka])
        # Wywołaj rysowanie bloku. Dla parzystych indeksów głębokość = 3,
        # dla nieparzystych głębokośc = 2 (drugi argument)
        rysuj_blok(1, (3, 2)[kwadrat % 2])

    turtle.hideturtle() # Schowaj żółwia

motyw()

input()
